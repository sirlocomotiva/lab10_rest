package ro.ubb.catalog.core.service;

import java.util.List;
import ro.ubb.catalog.core.model.*;

public interface RentService {
    public List<Rent> getAllRents();
    public Rent addRent(Rent c);
    public Rent updateRent(Long id, Rent c);
    public void deleteRent(Long id);
}
