package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.core.repository.RentRepository;

import java.util.List;
@Service
public class RentServiceServiceImpl implements RentService {
    public static final Logger log = LoggerFactory.getLogger(RentServiceServiceImpl.class);
    @Autowired
    private RentRepository rentRepository;

    @Override
    public List<Rent> getAllRents() {
        return rentRepository.findAll();
    }

    @Override
    public Rent addRent(Rent c) {

        return rentRepository.save(c);
    }

    @Override
    public Rent updateRent(Long id, Rent c) {
        log.trace("updateRent - method entered: client={}", c);
        Rent update = rentRepository.findById(id).orElse(c);
        update.setRentedToClientId(c.getRentedToClientId());
        update.setMovieRentedId(c.getMovieRentedId());
        update.setStartDate(c.getStartDate());
        log.trace("updateRent - method finished");
        return update;

    }

    @Override
    public void deleteRent(Long id) {
        rentRepository.deleteById(id);
    }
}
