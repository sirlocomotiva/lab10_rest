package ro.ubb.catalog.core.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepository;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client addClient(Client c) {

        return clientRepository.save(c);
    }

    @Override
    @Transactional
    public Client updateClient(Long id, Client c) {
        log.trace("updateClient - method entered: client={}", c);
        Client update = clientRepository.findById(id).orElse(c);
        update.setName(c.getName());
        update.setCnp(c.getCnp());
        log.trace("updateClient - method finished");
        return update;

    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }
}
