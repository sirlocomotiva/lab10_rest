package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.repository.MovieRepository;
import ro.ubb.catalog.core.model.*;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    public static final Logger log = LoggerFactory.getLogger(MovieServiceImpl.class);

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    @Override
    public Movie addMovie(Movie c) {

        return movieRepository.save(c);
    }

    @Override
    @Transactional
    public Movie updateMovie(Long id, Movie c) {
        log.trace("updateMovie - method entered: client={}", c);
        Movie update = movieRepository.findById(id).orElse(c);
        update.setName(c.getName());
        update.setTimesRented(c.getTimesRented());
        log.trace("updateMovie - method finished");
        return update;

    }

    @Override
    public void deleteMovie(Long id) {
        movieRepository.deleteById(id);
    }
}
