package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Rent extends BaseEntity<Long> {
    private long rentedToClientId;
    private long movieRentedId;
    private String startDate;
}
