package ro.ubb.catalog.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.MoviesDto;

import java.util.Scanner;

public class ClientApp {
    public static void main(String[] args) {
        final String ClientURL = "http://localhost:8080/api/clients";
        final String MovieURL = "http://localhost:8080/api/movies";
        final String RentURL = "http://localhost:8080/api/rents";
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.catalog.client.config"
                );

        RestTemplate restTemplate = context.getBean(RestTemplate.class);

        try {
            System.out.println("1.Manage client");
            System.out.println("2.Manage movie");
            System.out.println("3.Manage Rent");
            System.out.println("0.Exit");

            Scanner in = new Scanner(System.in);
            int option = in.nextInt();
            if (option == 1){
                System.out.println("1. Add client");
                System.out.println("2. Update client");
                System.out.println("3. Delete client");
                System.out.println("4. See all clients");
                option = in.nextInt();
                if (option == 1){
                    System.out.print("id: ");
                    Long id = in.nextLong();

                    System.out.print("cnp: ");
                    String cnp = in.next();

                    System.out.print("\n\tname: ");
                    String name = in.next();

                    ClientDto savedClient = restTemplate.postForObject(
                            ClientURL,
                            new ClientDto(name, cnp),
                            ClientDto.class);
                    System.out.println("savedClient: " + savedClient);
                }
                if (option == 2){
                    System.out.print("id: ");
                    Long id = in.nextLong();

                    System.out.print("cnp: ");
                    String cnp = in.next();

                    System.out.print("name: ");
                    String name = in.next();

                    ClientDto savedClient = new ClientDto();
                    savedClient.setName(name);
                    savedClient.setCnp(cnp);
                    savedClient.setId(id);

                    restTemplate.put(ClientURL + "/{id}", savedClient, savedClient.getId());
                }
                if (option == 3) {
                    System.out.print("id: ");
                    Long id = in.nextLong();
                    ClientDto savedClient = new ClientDto();
                    savedClient.setId(id);
                    restTemplate.delete(ClientURL + "/{id}", savedClient.getId());
                }
                if (option == 4) {
                    ClientsDto allClients = restTemplate.getForObject(ClientURL, ClientsDto.class);
                    System.out.println(allClients);
                }
            }
            if (option == 2){
                System.out.println("\t1. Add movie");
                System.out.println("\t2. Update movie");
                System.out.println("\t3. Delete movie");
                System.out.println("\t4. See all movies");
                option = in.nextInt();
                if (option == 1){
                    System.out.print("id: ");
                    Long id = in.nextLong();

                    System.out.print("\nname: ");
                    String name = in.next();

                    System.out.print("\ntimes rented: ");
                    int timesRented = in.nextInt();

                    MovieDto savedMovie = restTemplate.postForObject(
                            MovieURL,
                            new MovieDto(name, timesRented),
                            MovieDto.class);
                    System.out.println("savedClient: " + savedMovie);
                }
                if (option == 2) {
                    System.out.print("\tid: ");
                    Long id = in.nextLong();

                    System.out.print("\n\tname: ");
                    String name = in.next();

                    System.out.print("\n\ttimes rented: ");
                    int timesRented = in.nextInt();

                    MovieDto savedMovie = new MovieDto();
                    savedMovie.setId(id);
                    savedMovie.setName(name);
                    savedMovie.setTimesRented(timesRented);
                    restTemplate.put(MovieURL + "/{id}", savedMovie, savedMovie.getId());

                }
                if (option == 3) {
                    System.out.print("\tid: ");
                    Long id = in.nextLong();

                    MovieDto savedMovie = new MovieDto();
                    savedMovie.setId(id);
                    restTemplate.delete(ClientURL + "/{id}", savedMovie.getId());
                }
                if (option == 4) {
                    MoviesDto allMovies = restTemplate.getForObject(MovieURL, MoviesDto.class);
                    System.out.println(allMovies);
                }
            }
            /*
            //RENT
            if (option == 3){
                System.out.println("\t1. Add rent");
                //System.out.println("\t2. Update rent");
                //System.out.println("\t3. Delete rent ");
                System.out.println("\t4. See all rents");
                option = in.nextInt();
                if (option == 1){
                    System.out.print("\tid: ");
                    Long id = in.nextLong();

                    System.out.print("\n\tclientId: ");
                    Long clientId = in.nextLong();

                    System.out.print("\n\tmovieId: ");
                    Long movieId = in.nextLong();

                    System.out.print("\n\tStart Date: ");
                    String startDate = in.next();

                    rentService.addRent(new Rent(id, clientId, movieId, startDate));
                }
                if (option == 2){
                    System.out.print("\tid: ");
                    Long id = in.nextLong();

                    //Rent rent = rentService.updateRent(new Rent());
                    //System.out.println(rent);
                }
                if (option == 3) {
                    System.out.print("\tid: ");
                    Long id = in.nextLong();
                    //rentService.deleteRentById(id);
                }
                if (option == 4) {
                    rentService.getAllRents().forEach(System.out::println);
                }
            }

             */
            if (option == 0) {
                System.out.println("Adios!");
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
