package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.MovieDto;
@Component
public class MovieConverter extends BaseConverter<Movie, MovieDto> {
    @Override
    public Movie convertDtoToModel(MovieDto dto) {
        /*Client client = Client.builder()
                .name(dto.getName())
                .cnp(dto.getCnp())
                .build();
        client.setId(dto.getId());
        return client;*/
        Movie movie = Movie.builder()
                .name(dto.getName())
                .timesRented(dto.getTimesRented())
                .build();
        movie.setId(dto.getId());
        return movie;
    }

    @Override
    public MovieDto convertModelToDto(Movie movie) {
        /*ClientDto dto = ClientDto.builder()
                .name(client.getName())
                .cnp(client.getCnp())
                .build();
        dto.setId(client.getId());
        return dto;*/
        MovieDto dto = MovieDto.builder()
                .name(movie.getName())
                .timesRented(movie.getTimesRented())
                .build();
        dto.setId(movie.getId());
        return dto;
    }
}
