package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.RentService;
import ro.ubb.catalog.web.converter.RentConverter;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.MoviesDto;
import ro.ubb.catalog.web.dto.RentDto;
import ro.ubb.catalog.web.dto.RentsDto;

@RestController
public class RentController {

    @Autowired
    private RentService rentService;

    @Autowired
    private RentConverter rentConverter;

    @RequestMapping(value = "/rents", method = RequestMethod.GET)
    RentsDto getRents() {
        //todo: log
        return new RentsDto(rentConverter
                .convertModelsToDtos(rentService.getAllRents()));

    }

    @RequestMapping(value = "/rents", method = RequestMethod.POST)
    RentDto saveRent(@RequestBody RentDto rentDto) {
        //todo log
        return rentConverter.convertModelToDto(rentService.addRent(
                rentConverter.convertDtoToModel(rentDto)
        ));
    }

    @RequestMapping(value = "/rents/{id}", method = RequestMethod.PUT)
    RentDto updateRent(@PathVariable Long id,
                       @RequestBody RentDto rentDto) {
        //todo: log
        return rentConverter.convertModelToDto(rentService.updateRent(id,
                rentConverter.convertDtoToModel(rentDto)));
    }

    @RequestMapping(value = "/rents/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        //todo:log

        rentService.deleteRent(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
