package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.web.dto.RentDto;
@Component
public class RentConverter extends BaseConverter<Rent, RentDto> {
    @Override
    public Rent convertDtoToModel(RentDto dto) {
        Rent rent = Rent.builder()
                .rentedToClientId(dto.getRentedToClientId())
                .movieRentedId(dto.getMovieRentedId())
                .startDate(dto.getStartDate())
                .build();
        rent.setId(dto.getId());
        return rent;
    }

    @Override
    public RentDto convertModelToDto(Rent rent) {
        RentDto dto = RentDto.builder()
                .rentedToClientId(rent.getRentedToClientId())
                .movieRentedId(rent.getMovieRentedId())
                .startDate(rent.getStartDate())
                .build();
        dto.setId(rent.getId());
        return dto;
    }
}
